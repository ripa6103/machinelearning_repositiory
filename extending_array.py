import numpy as np
class DynamicArr:

    def __init__(self, dim_1, dim_2, dynamic_dim):
        self._shape_dim_1 = dim_1
        self._shape_dim_2 = dim_2
        self._predefined_size = 100
        self._dynamic_dim = dynamic_dim
        self.data = None
        self.previous_marker = 0
        self.current_marker = 0
        self._capacity = self._predefined_size
        self.data = self._assign_or_extend_space()

    def _assign_or_extend_space(self):
        if isinstance(self.data, np.ndarray):
            if self._dynamic_dim == "dym1":
                new_data = np.zeros((self.data.shape[0] + self._predefined_size, self._shape_dim_2))
                new_data[:self.data.shape[0], :] = self.data
            if self._dynamic_dim == "dym2":
                new_data = np.zeros((self._shape_dim_1, self.data.shape[1] + self._predefined_size))
                new_data[:, :self.data.shape[1]] = self.data
            self._capacity += self._predefined_size
            self.data = new_data
        else:
            if self._dynamic_dim == "dym1":
                self.data = np.zeros((self._predefined_size, self._shape_dim_2))
            if self._dynamic_dim == "dym2":
                self.data = np.zeros((self._shape_dim_1, self._predefined_size))

        return self.data

    def update(self, new_entry):
        if self.current_marker == self._capacity:
            self._assign_or_extend_space()

        if self._dynamic_dim == "dym1":
            self.data[self.current_marker, :] = new_entry
        if self._dynamic_dim == "dym2":
            self.data[:, self.current_marker] = new_entry
        self.current_marker += 1

    def extract_data(self):
        if self.previous_marker == self.current_marker:
            return None
        else:
            if self._dynamic_dim == "dym1":
                d = self.data[self.previous_marker:self.current_marker, :]
            if self._dynamic_dim == "dym2":
                d = self.data[:, self.previous_marker:self.current_marker]

            self.previous_marker = self.current_marker
            return d


class DynamicStructuredArray:

    def __init__(self, dim_1, dim_2, dynamic_dim):
        self._shape_dim_1 = dim_1
        self._shape_dim_2 = dim_2
        self._predefined_size = 100
        self._dynamic_dim = dynamic_dim
        self.data = None
        self.previous_marker = 0
        self.current_marker = 0
        self._capacity = self._predefined_size
        self.data = self._assign_or_extend_space()

    def _assign_or_extend_space(self):
        if isinstance(self.data, np.ndarray):
            if self._dynamic_dim == "dym1":
                new_data = np.zeros((self.data.shape[0] + self._predefined_size, self._shape_dim_2))
                new_data[:self.data.shape[0], :] = self.data
            if self._dynamic_dim == "dym2":
                new_data = np.zeros((self._shape_dim_1, self.data.shape[1] + self._predefined_size))
                new_data[:, :self.data.shape[1]] = self.data
            self._capacity += self._predefined_size
            self.data = new_data
        else:
            if self._dynamic_dim == "dym1":
                self.data = np.zeros((self._predefined_size, self._shape_dim_2))
            if self._dynamic_dim == "dym2":
                self.data = np.zeros((self._shape_dim_1, self._predefined_size))

        return self.data

    def update(self, new_entry):
        if self.current_marker == self._capacity:
            self._assign_or_extend_space()

        if self._dynamic_dim == "dym1":
            self.data[self.current_marker, :] = new_entry
        if self._dynamic_dim == "dym2":
            self.data[:, self.current_marker] = new_entry
        self.current_marker += 1

    def extract_data(self):
        if self.previous_marker == self.current_marker:
            return None
        else:
            if self._dynamic_dim == "dym1":
                d = self.data[self.previous_marker:self.current_marker, :]
            if self._dynamic_dim == "dym2":
                d = self.data[:, self.previous_marker:self.current_marker]

            self.previous_marker = self.current_marker
            return d