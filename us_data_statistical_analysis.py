"""This class can benefit in following:

a) Give excellent ideas on how to code without using a for loop
b) Exercise some Data Analysis skills
c) Show how to do local interpolations using python libraries
d) Give example of GP Regression widely used in Machine Learning

e) Validity of scans are checked based on a statistical estimator using mahalanobis distance
6) Based of the clustering : Similarity between measurements close to defects can be observed"""


import numpy as np
from collections import defaultdict, Counter
import pickle
import os
from extending_array import DynamicArr
from itertools import product
# Trying interpolate functions of Scipy
from scipy.interpolate import griddata

# Trying GP with predefined kernels
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, ConstantKernel, ExpSineSquared
from keras.models import model_from_json

from pathlib import Path


class DNN_Based_Analysis:

    def __init__(self, model_filename, model_wts_filename, optimizer='adam', custom_objects=None):
        self.model = self.load_model(model_filename, model_wts_filename, optimizer, custom_objects)

    def pad_zeros(self, data):
        if data is not None:
            data_shape = data.shape
            do_padding = False
            if data_shape[1] is 10 and data_shape[2] is 10:
                if data_shape[0] < 590:
                    do_padding = True

            assert do_padding, ValueError("Shape of data is not fit for neural network")
            padded_data = np.pad(data, ((0, 590 - data_shape[0]), (0, 0), (0, 0)), 'edge')
            return padded_data
        else:
            return None

    def pad_zeros_2D(self, data):
        return np.pad(data, ((0, 0), (0, 590 - data.shape[1])))

    def predict_data(self, data):
        if data is not None:
            data = data.transpose(1, 2, 0)
            data = data[np.newaxis, :, :, :]
            assert self.model is not None, ValueError("Model is not defined")
            assert data.shape[-1] is not 590 or data.shape[-2] is not 10 and data.shape[-3] is not 10, ValueError(
                "Shape of data is is not fit for loaded model")
            predicted_clusters = self.model.predict(data)
            predicted_clusters = predicted_clusters.squeeze()
            predicted_clusters = predicted_clusters.transpose(2, 0, 1)

            # return predicted_clusters[:580, :, :]
            return predicted_clusters
        else:
            return None

    def load_model(self, model_json_file, model_weights, optimizer, custom_objects=None):
        rel_path = ""
        model_json_file = rel_path + model_json_file
        model_weights = rel_path + model_weights
        json_file = open(model_json_file, 'r')
        loaded_model_json = json_file.read()
        json_file.close()

        loaded_model = model_from_json(loaded_model_json, custom_objects)
        loaded_model.load_weights(model_weights)

        loaded_model.compile(loss='mean_squared_error', optimizer=optimizer, metrics=['accuracy'])
        return loaded_model


class StatisticalDataDescriptor:
    this_path = Path(__file__)
    root_path = this_path.parent
    raw_measurement_path = root_path.joinpath("measurements/")

    reconstructed_measurement_path = root_path.joinpath("reconstructions/")
    interpolated_measurement_path = root_path.joinpath("interpolated_measurements/")

    mcd_path = root_path.joinpath("minimum_covariance_determinant_matrix/")

    dnn_model_path = root_path.joinpath("DNN_Model_and_weights/")
    dnn_model_filename = "model_s_sfilt_unet_1.json"
    model_weights_filename = "wt_unet_s_sfilt_model_4.hdf5"
    dnn_optimizer = "adam"

    def __init__(self, measurement_filename):
        # A region dictionary
        self.classified_measurements = defaultdict(list)
        self.classified_positions = defaultdict(list)
        self.classified_regions = defaultdict(list)

        # Region boundaries
        self.x_box = 10
        self.y_box = 10

        # mcd calculator
        self.mcd = self.load_mcd("mcd_100_400.mat")

        # load measurement and position
        self.measurements, self.positions = self.load_measurement(measurement_filename)

        # self.dnn = DNN_Based_Analysis(self.dnn_model_filename, self.model_weights_filename, self.dnn_optimizer)
        # self.padded_measurements = self.dnn.pad_zeros_2D(self.measurements)

        # dynamic measurement and position storage (Real time simulation)
        self.dyn_array_data = DynamicArr(None, self.measurements.shape[1], 'dym1')
        self.dyn_array_position = DynamicArr(None, self.positions.shape[1], 'dym1')

        self.defmap = self.create_empty_defmap()
        self.interpolated_dnn_measurements = self.create_empty_defmap()

        # Gp combination kernel --> RBF kernel has only length parameter which is length of the kernel. To also
        # incorporate the vertical variation (sigma_f) we need to multiply the RBF kernel with a constant kernel
        self.kernel = ConstantKernel(1.0) * RBF(length_scale=1.0) * ExpSineSquared(periodicity=3.0)
        self.noise = 0.5
        self.gpr = GaussianProcessRegressor(kernel=self.kernel, alpha=self.noise ** 2)

        self.local_points = self.local_mesh_for_interpolation()
        self.grid_x, self.grid_y = self.local_grid_points_for_interpolation()

        self.measurement_boundary_x = (self.positions[:, 0].min(), self.positions[:, 0].max())
        self.measurement_boundary_y = (self.positions[:, 1].min(), self.positions[:, 1].max())

    def create_empty_defmap(self):
        return np.zeros(
            (self.measurements.shape[1], self.positions[:, 0].max() + 1, self.positions[:, 1].max() + 1))

    def local_mesh_for_interpolation(self):
        local_points = np.array(list(product(np.arange(self.x_box), np.arange(self.y_box))))
        return local_points

    def local_grid_points_for_interpolation(self):
        return np.mgrid[0:self.x_box:1, 0:self.y_box:1]

    def covariance_func(self, measurement_vectors):
        """
        This function calculates the covariance between a bunch of measurements. These measurements are usually a
        collected in a defined neighbourhood

         measurement_vectors --> rows = number of measurements and cols = samples in measurement vector
        """
        slice1 = measurement_vectors[:, np.newaxis, :]
        slice1_t = measurement_vectors[np.newaxis, :, :]
        difference = slice1 - slice1_t  # should be a (num_meas x num_meas x num_samples) matrix
        squared_distance = difference ** 2
        difference_two_norm = np.sum(squared_distance, axis=2)  # sum is done along the samples in measurement axis
        exp_distance = np.exp(-difference_two_norm)
        return exp_distance

    def get_pythagoras_dist_bw_positions(self, positions_vector):
        """This function calculates the euclidean distance between the measurement points. The given measurement points
        also belong to a defined neighbourhood

        positions_vector --> rows = number of measurements and cols = 2 based on cartesian system
        """
        positions1 = positions_vector[:, np.newaxis, :]
        positions1_t = positions_vector[np.newaxis, :, :]
        simple_dist = positions1 - positions1_t  # This creates matrix of size (num_meas x num_meas x 2) simple_dist[
        # :, :, 0] = x - x1...xn and simple_dist[:, :, 1] = y - y1,...yn
        angle = np.arctan(simple_dist[:, :, 1] / simple_dist[:, :, 0])
        sq_dist = simple_dist ** 2  # sq_dist[:, :, 0] is X^2, sq_dist[:, :, 1] = Y^2
        pythagoras_dist = np.sqrt(np.sum(sq_dist, axis=2))  # gives pythagoras distance between all points
        return np.dstack((pythagoras_dist, angle))

    def load_measurement(self, filename):
        """Load an already acquired smart inspect measurement.
        filename : name of the npz file without extension"""
        path = self.raw_measurement_path.joinpath(filename + ".npz")
        loaded_data = np.load(path)
        positions = loaded_data['pos']
        positions = np.asarray(positions, dtype=np.int64)
        return loaded_data['data'], positions

    def save_interpolated_measurement(self, path, filename, data):
        path = path + filename
        np.savez_compressed(path, data=data)

    def allot_measurements_to_regions(self, x_strides, y_strides, pos, measurement):
        """This function separates incoming measurements into a defined neighbourhood based on position of acquisition.
        x_strides : length of neighbourhood on x-axis
        y_strides . length of neighbourhood on y-axis
        pos : position where measurement is acquired (x,y)
        measurement : ultrasound measurement (a-scan) at pos
        """

        self.classified_measurements[str(int(pos[0] // x_strides)) + ',' + str(int(pos[1] // y_strides))].append(
            measurement)
        self.classified_positions[str(int(pos[0] // x_strides)) + ',' + str(int(pos[1] // y_strides))].append(pos)

    def define_regions_based_on_points(self, coordinate):
        x_min = np.int(coordinate[0] - self.x_box // 2)
        x_max = np.int(x_min + self.x_box)
        y_min = np.int(coordinate[1] - self.y_box // 2)
        y_max = np.int(y_min + self.y_box)

        x_range, y_range = self.restrict_region_within_boundary([x_min, x_max], [y_min, y_max])
        return x_range, y_range

    def restrict_region_within_boundary(self, x_vals, y_vals):
        if x_vals[0] < self.measurement_boundary_x[0]:
            x_vals[0] = self.measurement_boundary_x[0]
            if x_vals[1] - x_vals[0] != self.x_box:
                x_vals[1] = x_vals[1] + (self.x_box - (x_vals[1] - x_vals[0]))
        if x_vals[1] > self.measurement_boundary_x[1]:
            x_vals[1] = self.measurement_boundary_x[1]
            if x_vals[1] - x_vals[0] != self.x_box:
                x_vals[0] = x_vals[0] - (self.x_box - (x_vals[1] - x_vals[0]))
        if y_vals[0] < self.measurement_boundary_y[0]:
            y_vals[0] = self.measurement_boundary_y[0]
            if y_vals[1] - y_vals[0] != self.y_box:
                y_vals[1] = y_vals[1] + (self.y_box - (y_vals[1] - y_vals[0]))
        if y_vals[1] > self.measurement_boundary_y[1]:
            y_vals[1] = self.measurement_boundary_y[1]
            if y_vals[1] - y_vals[0] != self.y_box:
                y_vals[0] = y_vals[0] - (self.y_box - (y_vals[1] - y_vals[0]))
        return x_vals, y_vals

    def extract_measurements_from_defmap(self, x_range, y_range):
        return self.defmap[:, x_range[0]:x_range[1], y_range[0]:y_range[1]]

    def divide_defmap_to_regions(self, defmap, min_number):
        non_zero_x, non_zero_y = np.nonzero(np.sum(defmap, axis=0))
        x_min = non_zero_x.min()
        x_max = non_zero_x.max()
        y_min = non_zero_y.min()
        y_max = non_zero_y.max()

        x_clust_array = np.arange(x_min, x_max - self.x_box + 1, self.x_box // 5, dtype=np.int64)
        y_clust_array = np.arange(y_min, y_max - self.y_box + 1, self.y_box // 5, dtype=np.int64)

        if ((x_max - x_min) % self.x_box > 5) and ((y_max - y_min) % self.y_box) > 5:
            x_clust_array = np.hstack((x_clust_array, np.array([x_max - self.x_box], dtype=np.int64)))
            y_clust_array = np.hstack((y_clust_array, np.array([y_max - self.y_box], dtype=np.int64)))

        self.index_list = list(product(x_clust_array, y_clust_array))
        """If i do product of two iterables the structure is --> (x1, all_y), (x2, all_y)"""
        defmap_clusters = list(
            map(lambda x: self.defmap[:, x[0]:x[0] + self.x_box, x[1]:x[1] + self.y_box], self.index_list))
        defmap_clusters = list(map(lambda x: self.check_legitimate_d_map(x, min_number), defmap_clusters))
        return defmap_clusters

    def put_back_interpolated_clusters(self, interpolated_clusters):
        if self.index_list is not None:
            self.interpolated_defmap = self.create_empty_defmap()
            indices = np.arange(len(self.index_list))
            l = list(map(lambda i: self.replace_meas(interpolated_clusters[i], self.index_list[i]), indices))

    def replace_meas(self, replacement_chunk, initial_pos):
        if replacement_chunk is not None:
            self.interpolated_defmap[:, initial_pos[0]:initial_pos[0] + self.x_box,
            initial_pos[1]:initial_pos[1] + self.y_box] += replacement_chunk

    def replace_measurement_in_region(self, x_range, y_range, replacement_data):
        if replacement_data is not None:
            self.interpolated_dnn_measurements[:, x_range[0]:x_range[1], y_range[0]:y_range[1]] += replacement_data

    def check_legitimate_d_map(self, d_map, min_number):
        x, y = np.nonzero(np.sum(d_map, axis=0))
        if len(x) > min_number:
            return d_map
        else:
            return None

    def calculate_variance_of_measurements(self, cluster_measurement):
        return np.var(cluster_measurement, axis=1)

    def smooth_chunk(self, min, max):
        if min != max:
            return np.arange(min, max + 1, dtype=np.int64)
        else:
            return np.array([], dtype=np.int64)

    def allot_measurements_to_defmap(self, pos, measurement):
        """Put the incoming measurements in the defmap"""
        self.defmap[:, pos[0], pos[1]] = measurement

    def information_region_of_cluster(self, defmap_cluster, jumps_threshold=10, useful_info_threshold=0.02):
        """It is usually observed that 90% of the measurements do not contain much information. And it is also very
        common to find similar measurements in a neighbourhood. Hence This function aims at finding locations in the
        bunch of neighbourhood a-scans where there is important information. There are some samples which pop up by
        mistake. These should be discarded. There are some samples which are missed out, there should be smoothed. The
        function is responsible for taking care of above logic and returning the samples where we should focus on for
        interpolating

        cluster_measurements : Ultrasound measurements in a defined neighbourhood (changing to defmap, we need to sum
        up the measurements in one of the axis to make it 2D. defmaps are 3D and axis 0 contains the measurements
        axis 1 = x-axis and axis 2 is y-axis. We smash the y-axis for the time being to make it n_samples x x_samples)
        jumps_threshold : if the adjacent information samples show a difference of more than threshold, it should be
        considered that there exist groups of information. We then smooth different group of informations
        useful_info_threshold : This represent the minimum variance above which we can find some information in the
        measurements
        """
        if defmap_cluster is not None:
            cluster_measurements_2D = np.sum(defmap_cluster, axis=2)
            cluster_variance = self.calculate_variance_of_measurements(cluster_measurements_2D)
            information_samples = np.where(cluster_variance > useful_info_threshold)[0]

            if len(information_samples) != 0 and len(information_samples) != 1:
                # check for jumps in the samples. Set a threshold above which smoothening is not possible
                difference_bw_adjacent_samples = information_samples[1:] - information_samples[:-1]
                where_jumps_grth_thresh = np.where(difference_bw_adjacent_samples > jumps_threshold)[0]
                if len(where_jumps_grth_thresh != 0):
                    if len(where_jumps_grth_thresh == 1):
                        chunk1 = information_samples[:where_jumps_grth_thresh[0] + 1]
                        chunk2 = information_samples[where_jumps_grth_thresh[0] + 1:]
                        sm_chunk1 = self.smooth_chunk(chunk1.min(), chunk1.max())
                        sm_chunk2 = self.smooth_chunk(chunk2.min(), chunk2.max())
                        stack = np.hstack((sm_chunk1, sm_chunk2))
                        if len(stack) > 2:
                            return stack
                        else:
                            return None
                    else:
                        final_stack = np.array([], dtype=np.int64)
                        for i in range(len(where_jumps_grth_thresh)):
                            if i == 0:
                                chunk = information_samples[:where_jumps_grth_thresh[i] + 1]
                            else:
                                chunk = information_samples[
                                        where_jumps_grth_thresh[i - 1] + 1:where_jumps_grth_thresh[i] + 1]
                            final_stack = np.hstack((final_stack, self.smooth_chunk(chunk.min(), chunk.max())))
                        chunk = information_samples[where_jumps_grth_thresh[len(where_jumps_grth_thresh) - 1] + 1:]
                        final_stack = np.hstack((final_stack, self.smooth_chunk(chunk.min(), chunk.max())))
                        if len(final_stack) > 2:
                            return final_stack
                        else:
                            return None

                else:
                    return self.smooth_chunk(information_samples.min(), information_samples.max())
            else:
                return None

    def extract_information_slices_from_data(self, defmap_cluster, slice_indices):
        # preprocessed_data are either clustered measurements or covariance of the clustered measurements. They are 3D
        # The data has to be of the format num_meas x num_meas x num_samples
        if slice_indices is None or defmap_cluster is None:
            return None
        return defmap_cluster[slice_indices, :, :]

    def interpolate_defmap(self, defmap_cluster):
        if defmap_cluster is not None:
            non_zero_x, non_zero_y = np.nonzero(np.max(defmap_cluster, axis=0))
            obs_x = np.c_[non_zero_x, non_zero_y]
            obs_y = defmap_cluster[:, obs_x[:, 0], obs_x[:, 1]]
            predictions, _ = self.gpr_prediction(obs_x, obs_y.T)
            return predictions

    def measure_signal_power(self, defmap_cluster):
        if defmap_cluster is not None:
            non_zero_x, non_zero_y = np.nonzero(np.max(defmap_cluster, axis=0))
            obs_x = np.c_[non_zero_x, non_zero_y]
            obs_y = defmap_cluster[:, obs_x[:, 0], obs_x[:, 1]]
            signal_power = obs_y ** 2
            signal_power_sum = np.sum(signal_power, axis=1) / signal_power.shape[1]
            return obs_x, obs_y, signal_power_sum.mean()

    def generate_noise(self, samples, signal_power, desired_snr):
        mean = 0
        signal_power_db = 10 * np.log10(signal_power)
        noise_db = signal_power_db - desired_snr
        noise_watts = 10 ** (noise_db / 10)
        variance = np.sqrt(noise_watts)
        return np.random.normal(mean, variance, (samples, self.x_box, self.y_box)), mean, variance

    def interpolate_defmap_with_interpolators(self, defmap_cluster, slice_indices):
        desired_SNR = 10  # in dB
        if defmap_cluster is not None and slice_indices is not None:
            obs_x, obs_y, signal_power = self.measure_signal_power(defmap_cluster)
            interpolated_defmap, mean, variance = self.generate_noise(defmap_cluster.shape[0], signal_power,
                                                                      desired_SNR)
            important_samples = obs_y[slice_indices, :]
            predicted_slices = np.array(list(
                map(lambda observations: self.interpolate_using_griddata(obs_x, observations, 'cubic'),
                    important_samples)))
            predicted_slices[np.isnan(predicted_slices)] = np.random.normal(mean, variance, len(
                predicted_slices[np.isnan(predicted_slices)]))
            interpolated_defmap[slice_indices, :, :] = predicted_slices
            return interpolated_defmap

    def interpolate_using_griddata(self, points, observations, method):
        return griddata(points, observations, (self.grid_x, self.grid_y), method)

    def interpolate_slices_using_gpr(self):
        pass

    def gpr_prediction(self, obs_x, obs_y):
        # implementation not complete
        self.gpr.fit(obs_x, obs_y)
        mu, cov = self.gpr.predict(self.local_points, return_cov=True)
        # np.random.multivariate_normal(mu, cov) adapt to interpolate for 2D slices
        mu_defmap = mu.reshape(self.x_box, self.x_box, obs_y.shape[1])
        mu_defmap = mu_defmap.transpose(2, 0, 1)
        return mu_defmap, cov

    def get_measurement_clusters(self, min_number, collection_dict):
        cluster_list = list(collection_dict.values())
        len_cluster_list = np.array(list(map(len, cluster_list)))
        filtered_cluster_list = list(map(cluster_list.__getitem__, np.where(len_cluster_list > min_number)[0]))
        filtered_cluster_array = list(map(np.array, filtered_cluster_list))
        return filtered_cluster_array

    def change_path(self, path):
        os.chdir(path)

    def load_mcd(self, file_name):
        self.change_path(self.mcd_path)
        mcd_file = pickle.load(open(file_name, "rb"))
        self.change_path(self.root_path)
        return mcd_file

    def understand_noise_distribution(self, bins):
        variance_sample_wise = np.var(self.measurements, axis=0)
        thresh = 0.00025
        var_thresh = np.where(variance_sample_wise < thresh)[0]
        noise_measurements = self.measurements[:, var_thresh]
        x, y = [], []
        for meas in noise_measurements:
            x_l, y_l = np.histogram(meas, bins)
            x.append(x_l)
            y.append(y_l)
        x = np.array(x)
        y = np.array(y)
        return x, y

    def real_time_simulation(self):
        min_number = 4
        valid_measurements = 0
        for i in range(len(self.positions)):
            if i % 100 == 0:
                print(i)
            meas = self.measurements[i, :]
            if np.sqrt(self.mcd.mahalanobis(meas[np.newaxis, :580])) > 40:
                valid_measurements += 1
                self.allot_measurements_to_regions(self.x_box, self.y_box, self.positions[i, :], meas)
                self.dyn_array_data.update(meas)
                self.dyn_array_position.update(self.positions[i, :])
                self.allot_measurements_to_defmap(pos=self.positions[i, :], measurement=meas)

                # if valid_measurements%3 == 0:
                #     # Calculate region based on current position
                #     x_range, y_range = self.define_regions_based_on_points(self.positions[i, :])
                #     # select region from the defmap
                #     sparse_region = self.extract_measurements_from_defmap(x_range, y_range)
                #     # predict measurement
                #     predicted_measurement = self.dnn.predict_data(sparse_region)
                #     # stitch prediction to defmap
                #     self.replace_measurement_in_region(x_range, y_range, predicted_measurement)

        self.clustered_measurements = self.get_measurement_clusters(min_number=min_number,
                                                                     collection_dict=self.classified_measurements)
        self.clustered_positions = self.get_measurement_clusters(min_number=min_number,
                                                                 collection_dict=self.classified_positions)

        self.cov_measurements = list(map(self.covariance_func, self.clustered_measurements))
        self.distance_measurements = list(map(self.get_pythagoras_dist_bw_positions, self.clustered_positions))

        self.defmap_clust = self.divide_defmap_to_regions(self.defmap, min_number)
        interpolated_defmap_clust = list(map(self.interpolate_defmap, self.defmap_clust))
        self.put_back_interpolated_clusters(interpolated_defmap_clust)
        # self.important_info_samples_list = list(map(self.information_region_of_cluster, self.defmap_clust))
        # self.interpolated_clusters = list(
        #     map(lambda x: self.interpolate_defmap_with_interpolators(self.defmap_clust[x],
        #                                                              self.important_info_samples_list[x]),
        #         np.arange(len(self.defmap_clust))))
        # self.put_back_interpolated_clusters(self.interpolated_clusters)
        # important_slices_to_interpolate = list(map(
        #     lambda ind: self.extract_information_slices_from_data(self.defmap_clust[ind],
        #                                                           self.important_info_samples_list[ind]),
        #     np.arange(len(self.defmap_clust))))

        return  # defmap_clust, self.interpolated_defmap, cov_measurements, pythagoras_dist
